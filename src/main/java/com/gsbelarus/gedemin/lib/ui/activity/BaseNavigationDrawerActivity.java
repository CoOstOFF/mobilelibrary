package com.gsbelarus.gedemin.lib.ui.activity;

import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.gedemin.gsbelarus.lib.R;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;

import java.util.ArrayList;
import java.util.List;

abstract public class BaseNavigationDrawerActivity extends BaseActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    private static final String TAG_STATE_SELECTED_POSITION = "tag_selected_navigation_drawer_position";

    private DrawerLayout drawer;
    private NavigationView navigationView;
    private View headerLayout;

    private int selectedNavItemId;
    private List<DrawerStateListener> drawerStateListeners = new ArrayList<>();
    private ActionBarDrawerToggle actionBarDrawerToggle;

    /**
     * Сonfiguration
     */

    protected int getDrawerIdResource() {
        return R.id.drawer;
    }

    protected int getNavViewIdResource() {
        return R.id.nav_view;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.gdmnlib_activity_drawer_base;
    }

    /**
     * наличие AppBarLayout с toolbar обязательно
     */
    @Override
    protected final boolean hasAppBar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        drawer = (DrawerLayout) findViewById(getDrawerIdResource());
        navigationView = (NavigationView) findViewById(getNavViewIdResource());

        setupHighLevelActivity();
        initNavHeaderData();
        findViewById(getContentFrameIdResource()).setFitsSystemWindows(false);

        if (savedInstanceState != null)
            selectedNavItemId = savedInstanceState.getInt(TAG_STATE_SELECTED_POSITION, -1);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        actionBarDrawerToggle.syncState();
    }

    protected void showDrawerForLearning(boolean force) {
        String prefName = PREF_USER_LEARNED_DRAWER + "_" + this.getClass().getSimpleName();
        boolean userLearnedDrawer = !force && getSharedBoolean(prefName, false);
        if (!userLearnedDrawer) {
            openDrawer();
            putSharedBoolean(prefName, true);
        }
    }

    @Override
    public void setToolbarTitle(@IdRes int navMenuItemId) {
        if (getSupportActionBar() != null) {
            MenuItem menuItem = navigationView.getMenu().findItem(navMenuItemId);
            if (menuItem != null)
                getSupportActionBar().setTitle(menuItem.getTitle());
            else
                LogUtil.i("NavViewItem with id=" + navMenuItemId + " not found");
        }
    }

    @Override
    public void setupHighLevelActivity() {
        super.setupHighLevelActivity();

        setupNavDrawer();
    }

    private void setupNavDrawer() {
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.gdmnlib_app_name, R.string.gdmnlib_app_name) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);

                for (DrawerStateListener listener : drawerStateListeners)
                    listener.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                for (DrawerStateListener listener : drawerStateListeners)
                    listener.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                for (DrawerStateListener listener : drawerStateListeners)
                    listener.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);

                for (DrawerStateListener listener : drawerStateListeners)
                    listener.onDrawerStateChanged(newState);
            }
        };

        drawer.setDrawerListener(actionBarDrawerToggle);
        drawer.setDrawerShadow(R.drawable.gdmnlib_drawer_shadow, Gravity.LEFT);

        actionBarDrawerToggle.setDrawerIndicatorEnabled(true); //burger
        actionBarDrawerToggle.syncState();

        navigationView.getLayoutParams().width = calculateNavViewWidth();
        navigationView.inflateMenu(getDrawerMenuResource());
        headerLayout = getLayoutInflater().inflate(getNavHeaderLayoutResource(), navigationView, false);
        navigationView.addHeaderView(headerLayout);
        navigationView.setNavigationItemSelectedListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if ((getWindow().getAttributes().flags & WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS) == WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                headerLayout.getLayoutParams().height += getStatusBarHeight();
        }
    }

    public boolean onNavigationItemSelected(MenuItem menuItem) {
        onNavItemSelected(menuItem);
        return true;
    }

    public void openDrawer() {
        drawer.openDrawer(GravityCompat.START);
    }

    protected int calculateNavViewWidth() {
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        int widthPhone = Math.min(size.x, size.y);
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int maxNavViewWidth = Math.round(350 * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        int navViewWidth = widthPhone - Math.round(56 * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return Math.min(maxNavViewWidth, navViewWidth);
    }

    private int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    protected abstract int getDrawerMenuResource();

    protected abstract int getNavHeaderLayoutResource();

    protected abstract void initNavHeaderData();

    protected abstract void onNavItemSelected(MenuItem menuItem);

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(TAG_STATE_SELECTED_POSITION, selectedNavItemId);
    }

    public void selectNavItem(@IdRes int selectedItemId) {
        MenuItem menuItem = navigationView.getMenu().findItem(selectedItemId);
        if (menuItem != null) {
            if (menuItem.isCheckable()) {
                selectedNavItemId = selectedItemId;
                menuItem.setChecked(true);
            }
        } else
            LogUtil.i("NavViewItem with id=" + selectedItemId + " not found");
    }

    @Override
    public void onBackPressed() {
        if (isOpenedDrawer()) {
            closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    public void closeDrawer() {
        drawer.closeDrawers();
    }

    public boolean isOpenedDrawer() {
        return drawer.isDrawerOpen(Gravity.LEFT);
    }

    public DrawerLayout getDrawer() {
        return drawer;
    }

    public ActionBarDrawerToggle getActionBarDrawerToggle() {
        return actionBarDrawerToggle;
    }

    public View getNavHeaderLayout() {
        return headerLayout;
    }

    public NavigationView getNavView() {
        return navigationView;
    }

    public int getSelectedNavItemId() {
        return selectedNavItemId;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle != null && actionBarDrawerToggle.onOptionsItemSelected(item))
            return true;

        if (item.getItemId() == android.R.id.home) {
            openDrawer();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addDrawerStateListener(DrawerStateListener listener) {
        drawerStateListeners.add(listener);
    }

    public void removeDrawerStateListener(DrawerStateListener listener) {
        drawerStateListeners.remove(listener);
    }

    public void clearDrawerStateListener() {
        drawerStateListeners.clear();
    }

    public abstract static class DrawerStateListener {

        public void onDrawerSlide(View drawerView, float slideOffset) {}

        public void onDrawerOpened(View drawerView) {}

        public void onDrawerClosed(View drawerView) {}

        public void onDrawerStateChanged(int newState) {}

        protected final Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        public final boolean equals(Object o) {
            return super.equals(o);
        }

        protected final void finalize() throws Throwable {
            super.finalize();
        }

        public final int hashCode() {
            return super.hashCode();
        }

        public final String toString() {
            return super.toString();
        }
    }
}