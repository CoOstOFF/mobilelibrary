package com.gsbelarus.gedemin.lib.ui;

import android.app.Application;
import android.content.Intent;

import com.gsbelarus.gedemin.lib.sync.protocol.AlarmReceiver;

abstract public class BaseApplication extends Application {

    /**
     * Необходим для создания синхронизации по расписанию.
     * Для работы необходимо расширить класс AlarmReceiver
     *
     * @return AlarmReceiver
     */
    abstract public AlarmReceiver getAlarmReceiver();

    @Override
    public void onCreate() {
        super.onCreate();

        AlarmReceiver alarmReceiver = getAlarmReceiver();
        if (alarmReceiver != null)
            alarmReceiver.onReceive(getApplicationContext(), new Intent("alarm_receiver_intent").putExtra(AlarmReceiver.ALARM_SYNC_START, false));
    }
}
