package com.gsbelarus.gedemin.lib.ui.utils;

import android.content.Context;

import com.gedemin.gsbelarus.lib.R;

import java.util.Calendar;

final public class CalendarHelper {

    /**
     * Локализованное имя месяца. Имена хранятся в xml файле strings в массиве month_list_values
     */
    public static String getMonthName(Context context, int month) {
        String[] months = context.getResources().getStringArray(R.array.gdmnlib_month_list_values);
        return months[month];
    }

    /**
     * Короткие локализованное имя месяца. Имена хранятся в xml файле strings в массиве short_month_list_values
     */
    public static String getShortMonthName(Context context, int month) {
        String[] months = context.getResources().getStringArray(R.array.gdmnlib_short_month_list_values);
        return months[month];
    }

    /**
     * Инициализирует поля Calendar до поля lastField значением value
     *
     * @param calendar  календарь с полями
     * @param lastField поле до которого будем помещать значение
     * @param value     значение которе будет помещено в поле
     */
    public static Calendar initCalendarFields(Calendar calendar, int lastField, int value) {
        int[] fields = {Calendar.MILLISECOND, Calendar.SECOND, Calendar.MINUTE, Calendar.HOUR_OF_DAY,
                Calendar.DAY_OF_MONTH, Calendar.MONTH, Calendar.YEAR};
        for (int field : fields) {
            if (field == lastField) break;
            calendar.set(field, value);
        }
        return calendar;
    }
}
