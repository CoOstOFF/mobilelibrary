package com.gsbelarus.gedemin.lib.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.gedemin.gsbelarus.lib.R;
import com.gsbelarus.gedemin.lib.ui.utils.BundleHelper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class BaseFragment extends Fragment {

    private static final ExecutorService executor = Executors.newFixedThreadPool(1);

    private static final String TAG_SHOWING_PROGRESS_BAR = "tag_showing_progress_bar";

    private volatile boolean fragmentDestroy;
    private boolean alreadyLoaded;
    private boolean loadingFragmentData;
    private boolean showingProgressBar;

    private Context context;
    private View rootView;
    private ProgressBar progressBar;
    private FrameLayout contentView;

    @LayoutRes
    protected abstract int getLayoutResource();

    protected void loadFragmentDataToBackend() {
    }

    /**
     * * This will be called between
     * {@link #onCreateView(View, Bundle)} and {@link #onActivityCreated(Bundle)}
     * or after {@link #reloadFragmentData(boolean)} (boolean)}
     * <p/>
     * <p>{@link #onDestroyView} when the view is being released.
     *
     * @param rootView View of fragment
     */
    protected void initView(View rootView) {
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>{@link #onDestroyView} when the view is being released.
     *
     * @param rootView           View of fragment,
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     */
    public void onCreateView(View rootView, Bundle savedInstanceState) {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getActivity().getApplicationContext();
        fragmentDestroy = false;
        alreadyLoaded = false;
        loadingFragmentData = false;
        showingProgressBar = true;

        if (BundleHelper.containsArguments(savedInstanceState, TAG_SHOWING_PROGRESS_BAR))
            showingProgressBar = BundleHelper.get(savedInstanceState, TAG_SHOWING_PROGRESS_BAR);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        fragmentDestroy = true;
    }

    @Nullable
    @Override
    final public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.gdmnlib_frame_fragment_base, container, false);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
        contentView = (FrameLayout) rootView.findViewById(R.id.fragment_content_frame);
        contentView.addView(inflater.inflate(getLayoutResource(), contentView, false));

        onCreateView(rootView, savedInstanceState);

        if (!alreadyLoaded)
            reloadFragmentData(showingProgressBar);
        else
            initView(rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void reloadFragmentData() {
        reloadFragmentData(showingProgressBar);
    }

    private void reloadFragmentData(boolean withProgressBar) {
        loadingFragmentData = true;
        showProgressBar(withProgressBar);
        final Handler handler = new Handler();
        executor.execute(new Runnable() {
            @Override
            public void run() {

                if (fragmentDestroy) return;
                loadFragmentDataToBackend();

                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        alreadyLoaded = true;
                        loadingFragmentData = false;
                        if (isAdded() && !fragmentDestroy) {
                            showProgressBar(false);
                            initView(rootView);
                        }
                    }
                });
            }
        });
    }

    private void showProgressBar(boolean visibility) {
        if (progressBar != null && contentView != null) {
            progressBar.setVisibility((visibility) ? View.VISIBLE : View.GONE);
            contentView.setVisibility((visibility) ? View.INVISIBLE : View.VISIBLE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(TAG_SHOWING_PROGRESS_BAR, showingProgressBar);
    }

    @Nullable
    public View getRootView() {
        return rootView;
    }

    public boolean isShowingProgressBar() {
        return showingProgressBar;
    }

    public void setShowingProgressBar(boolean showingProgressBar) {
        this.showingProgressBar = showingProgressBar;
    }

    public boolean isLoadingFragmentData() {
        return loadingFragmentData;
    }

    public boolean isFragmentDestroy() {
        return fragmentDestroy;
    }

    @Override
    public Context getContext() {
        return getActivity() == null ? context : getActivity().getApplicationContext();
    }
}
