package com.gsbelarus.gedemin.lib.ui.activity;

import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.gedemin.gsbelarus.lib.R;
import com.gsbelarus.gedemin.lib.sync.protocol.SyncServiceModel;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncServiceStatus;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

abstract public class BaseActivity extends AppCompatActivity {
    /**
     * TAG for logging
     */
    protected final String TAG = getClass().getCanonicalName();

    protected Context context;

    private Toolbar toolbar;
    private AppBarLayout appBarLayout;

    private boolean isActivityVisible;
    private Dialog syncStatusDialog;
    private BroadcastReceiver broadcastReceiver;

    /**
     * Сonfiguration
     */

    protected int getLayoutResource() {
        return R.layout.gdmnlib_activity_base;
    }

    public int getContentFrameIdResource() {
        return R.id.activity_content_frame;
    }

    //TODO @Nullable
    protected int getContentFragmentPlaceIdResource() {
        return R.id.activity_content_fragment_place;
    }

    /**
     * если AppBarLayout (toolbar должен находиться именно в нем) отсутствует,
     * достаточно переопределить только этот метод
     */
    protected boolean hasAppBar() {
        return true;
    }

    @Nullable
    protected Integer getAppBarLayoutResource() {
        return hasAppBar() ? R.layout.gdmnlib_appbar : null;
    }

    @Nullable
    protected Integer getToolbarIdResource() {
        return hasAppBar() ? R.id.toolbar : null;
    }

    public void onReceiveSyncStatus(SyncServiceStatus syncServiceStatus, SyncStatusDialog syncStatusDialog) {
    }

    /**
     * необходимо переопределить и вызвать setupHighLevelActivity || setupSubActivity() || setupSubActivityWithTitle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutResource());

        context = getBaseContext();
        createBroadcastReceiver();
        setupContentFragment();
    }

    /**
     * ловит и обрабатывает широковещательные сообщения от сервиса обновлений
     */
    private void createBroadcastReceiver() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getExtras().containsKey(SyncServiceStatus.class.getSimpleName())) {
                    SyncServiceStatus syncServiceStatus =
                            (SyncServiceStatus) intent.getExtras().getSerializable(SyncServiceStatus.class.getSimpleName());

                    if (syncServiceStatus != null &&
                            syncServiceStatus.getTypeOfServiceStatus() == SyncServiceStatus.TypeOfServiceStatus.START_SYNC) {

                        if (syncStatusDialog != null)
                            syncStatusDialog.cancel();
                    }
                    onReceiveSyncStatus(syncServiceStatus, new SyncStatusDialog(syncServiceStatus));
                }
            }
        };

        IntentFilter syncIntentFilter = new IntentFilter(SyncServiceModel.getBroadcastMessageAction(getApplicationContext()));
        registerReceiver(broadcastReceiver, syncIntentFilter);
    }

    public class SyncStatusDialog {

        private SyncServiceStatus status;

        public SyncStatusDialog(SyncServiceStatus status) {
            this.status = status;
        }

        public void showDialog(Dialog dialog) {
            String message = status.getStatus().getMessage();
            if (!isActivityVisible || message == null || message.isEmpty())
                return;
            syncStatusDialog = dialog;
            if (!BaseActivity.this.isFinishing())
                syncStatusDialog.show();
        }

        public void showAlert() {
            showDialog(new AlertDialog.Builder(BaseActivity.this)
                    .setTitle(R.string.gdmnlib_sync_status_notification)
                    .setMessage(status.getStatus().getMessage())
                    .setPositiveButton(R.string.gdmnlib_dialog_ok, null)
                    .create());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        setActivityVisible(false);
    }

    private void setActivityVisible(boolean flag) {
        isActivityVisible = flag;
        SyncServiceModel.isShowNotificationError = !flag;
    }

    public boolean isVisible() {
        return isActivityVisible;
    }

    @Override
    protected void onResume() {
        super.onResume();

        setActivityVisible(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (syncStatusDialog != null)
            syncStatusDialog.cancel();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    protected void setupHighLevelActivity() {
        if (hasAppBar()) {
            setupAppBar();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false); //TODO ?
        }
    }

    private void setupAppBar() {
        ViewGroup contentFrame = (ViewGroup) findViewById(getContentFrameIdResource());

        appBarLayout = (AppBarLayout) getLayoutInflater().inflate(getAppBarLayoutResource(), contentFrame, false);
        if (contentFrame instanceof LinearLayout)
            contentFrame.addView(appBarLayout, 0);
        else
            contentFrame.addView(appBarLayout);

        toolbar = (Toolbar) findViewById(getToolbarIdResource());
        setSupportActionBar(toolbar);

        ViewGroup contentFragment = (ViewGroup) findViewById(getContentFragmentPlaceIdResource());
        getLayoutInflater().inflate(R.layout.gdmnlib_view_appbar_shadow, contentFragment, true);

        setVisibilityAppBarShadow(true);
    }

    public void setVisibilityAppBarShadow(boolean visibility) {
        if (hasAppBar()) {
            View shadow = findViewById(R.id.appbar_shadow);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                if (shadow != null)
                    shadow.setVisibility((visibility) ? View.VISIBLE : View.GONE);
            } else {
                StateListAnimator stateListAnimator = new StateListAnimator(); // FIXME: 08.08.2016 
                if (visibility) {
                    stateListAnimator.addState(new int[0], ObjectAnimator.ofFloat(appBarLayout, "elevation", 16));
                } else {
                    stateListAnimator.addState(new int[0], ObjectAnimator.ofFloat(appBarLayout, "elevation", 0));
                }
                appBarLayout.setStateListAnimator(stateListAnimator);
//                appBarLayout.setElevation((visibility) ? appBarLayout.getTargetElevation() : 0);
            }
        }
    }

    protected void setupSubActivity() {
        if (hasAppBar()) {
            setupAppBar();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            getSupportActionBar().setDisplayUseLogoEnabled(false);
            getSupportActionBar().setShowHideAnimationEnabled(true);
        }
    }

    protected void setupSubActivityWithTitle() {
        setupSubActivity();
        if (hasAppBar())
            getSupportActionBar().setDisplayShowTitleEnabled(true);
    }

//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE ||
//                newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
//            if (hasAppBar() && toolbar != null) {
//                Context context = toolbar.getContext();
//                TypedArray typedArray = context.obtainStyledAttributes(new int[]
//                        {android.R.attr.actionBarStyle, android.R.attr.actionBarSize});
//                int actionStyle = typedArray.getResourceId(0, 0);
//                int actionHeight = typedArray.getDimensionPixelSize(1, 0);
//                typedArray.recycle();
//
//                toolbar.getLayoutParams().height = actionHeight;
//                toolbar.setMinimumHeight(actionHeight);
////                typedArray = context.obtainStyledAttributes(actionStyle, new int[]
////                        {android.R.attr.titleTextStyle});
////                toolbar.setTitleTextAppearance(context, typedArray.getResourceId(0, 0));
////                typedArray.recycle();
//            }
//        }
//    }

    public void setScrollFlagsToolbar(@ScrollFlags int flags) {
        if (hasAppBar() && toolbar != null) {
            ((AppBarLayout.LayoutParams) toolbar.getLayoutParams()).setScrollFlags(flags);
        }
    }

    @ScrollFlags
    public int getScrollFlagsToolbar() {
        if (hasAppBar() && toolbar != null) {
            return ((AppBarLayout.LayoutParams) toolbar.getLayoutParams()).getScrollFlags();
        }
        return 0;
    }

    public void setToolbarTitle(@StringRes int titleResId) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(titleResId);
    }

    @IntDef(flag = true,
            value = {AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL, AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP, AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED,
                    AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS, AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS_COLLAPSED})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ScrollFlags {
    }

    public void setToolbarTitle(CharSequence title) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(title);
    }

    @Nullable
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Nullable
    public AppBarLayout getAppBarLayout() {
        return appBarLayout;
    }

    public void addToAppBar(View view) {
        if (appBarLayout != null)
            appBarLayout.addView(view);
    }

    public void removeFromAppBar(View view) {
        if (appBarLayout != null)
            appBarLayout.removeView(view);
    }

    public void addToContentFrame(View view) {
        ViewGroup viewGroup = (ViewGroup) findViewById(getContentFrameIdResource());
        if (viewGroup != null)
            viewGroup.addView(view);
    }

    public void removeFromContentFrame(View view) {
        ViewGroup viewGroup = (ViewGroup) findViewById(getContentFrameIdResource());
        if (viewGroup != null)
            viewGroup.removeView(view);
    }

    private int setupContentFragment() {
        ViewGroup contentFrgm = (ViewGroup) findViewById(getContentFragmentPlaceIdResource());
        if (contentFrgm == null) return -1;
        FrameLayout contentFrgmShadow = (FrameLayout) contentFrgm.findViewById(R.id.activity_content_fragment_place_shadow);
        if (contentFrgmShadow == null) {
            contentFrgmShadow = (FrameLayout) getLayoutInflater().inflate(R.layout.gdmnlib_frame_layout, contentFrgm, false);
            contentFrgm.addView(contentFrgmShadow, 0);
        }
        return contentFrgmShadow.getId();
    }

    @SuppressWarnings("unchecked")
    @Nullable
    protected <T extends Fragment> T findSupportFragment(String tag) {
        return (T) getSupportFragmentManager().findFragmentByTag(tag);
    }

    @SuppressWarnings("unchecked")
    @Nullable
    protected <T extends android.app.Fragment> T findFragment(String tag) {
        return (T) getFragmentManager().findFragmentByTag(tag);
    }

    protected void includeFragment(@NonNull Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                .add(setupContentFragment(), fragment, tag)
                .commitAllowingStateLoss();
    }

    protected void includeFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .add(setupContentFragment(), fragment)
                .commitAllowingStateLoss();
    }

    protected void includeFragment(@NonNull android.app.Fragment fragment) {
        getFragmentManager().beginTransaction()
                .add(setupContentFragment(), fragment)
                .commitAllowingStateLoss();
    }

    protected void includeFragment(@NonNull android.app.Fragment fragment, String tag) {
        getFragmentManager().beginTransaction()
                .add(setupContentFragment(), fragment, tag)
                .commitAllowingStateLoss();
    }

    protected void removeFragment(String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment != null)
            getSupportFragmentManager().beginTransaction()
                    .remove(fragment)
                    .commitAllowingStateLoss();
    }

    protected void removeFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .remove(fragment)
                .commitAllowingStateLoss();
    }

    protected void removeFragment(@NonNull android.app.Fragment fragment) {
        getFragmentManager().beginTransaction()
                .remove(fragment)
                .commitAllowingStateLoss();
    }

    protected void replaceFragment(@NonNull Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                .replace(setupContentFragment(), fragment, tag)
                .commitAllowingStateLoss();
    }

    protected void replaceFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(setupContentFragment(), fragment)
                .commitAllowingStateLoss();
    }

    protected void replaceFragment(@NonNull android.app.Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(setupContentFragment(), fragment)
                .commitAllowingStateLoss();
    }

    protected void replaceFragment(@NonNull android.app.Fragment fragment, String tag) {
        getFragmentManager().beginTransaction()
                .replace(setupContentFragment(), fragment, tag)
                .commitAllowingStateLoss();
    }

    public void putSharedString(String prefKey, String defaultValue) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(prefKey, defaultValue);
        editor.apply();
    }

    public String getSharedString(String prefKey, String defaultValue) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(prefKey, defaultValue);
    }

    public void putSharedBoolean(String prefKey, boolean defaultValue) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(prefKey, defaultValue);
        editor.apply();
    }

    public boolean getSharedBoolean(String prefKey, boolean defaultValue) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(prefKey, defaultValue);
    }

    public void putSharedLong(String prefKey, long defaultValue) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(prefKey, defaultValue);
        editor.apply();
    }

    public long getSharedLong(String prefKey, long defaultValue) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getLong(prefKey, defaultValue);
    }

    public void putSharedInt(String prefKey, int defaultValue) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(prefKey, defaultValue);
        editor.apply();
    }

    public int getSharedInt(String prefKey, int defaultValue) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getInt(prefKey, defaultValue);
    }
}