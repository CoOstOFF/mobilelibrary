package com.gsbelarus.gedemin.lib.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.NumberPicker;

import com.gedemin.gsbelarus.lib.R;
import com.gsbelarus.gedemin.lib.ui.interfaces.OnDateSet;
import com.gsbelarus.gedemin.lib.ui.utils.CalendarHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

final public class DatePickerDialog {

    public static final String DEFAULT_TAG = "date_picker_dialog";

    private String tag = DEFAULT_TAG;

    private Context context;
    private OnDateSet listener;
    private AlertDialog dialog;
    private List<Calendar> dates;
    private int choiceMod;

    private int selectedDay;
    private int selectedMonth;
    private int selectedYear = 2014;

    private NumberPicker dayPicker;
    private NumberPicker monthPicker;
    private NumberPicker yearPicker;

    public void setOnDateSetListeners(OnDateSet listener) {
        this.listener = listener;
    }

    /**
     * Конструктор для создания диалога с выбором даты
     *
     * @param context   конетекст приложения
     * @param dates     массив дат, из которых доступен выбор.
     * @param choiceMod отображение определенных полей.
     *                  Calendar.DATE - отображаются все поля.
     *                  Calendar.MONTH - отображаются поля выбора месяца и года.
     *                  Calendar.YEAR - отображается только поле выбора года.
     */
    public DatePickerDialog(Context context, List<Calendar> dates, int choiceMod) {
        this.context = context;
        this.choiceMod = choiceMod;
        setDates(dates);

        View pickerView = View.inflate(context, R.layout.gdmnlib_date_picker, null);

        dialog = new AlertDialog.Builder(context)
                .setTitle("...")
                .setView(pickerView)
                .setPositiveButton(R.string.gdmnlib_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        updateDate();
                    }
                })
                .setNegativeButton(R.string.gdmnlib_dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create();

        dayPicker = (NumberPicker) pickerView.findViewById(R.id.day_picker);
        monthPicker = (NumberPicker) pickerView.findViewById(R.id.month_picker);
        yearPicker = (NumberPicker) pickerView.findViewById(R.id.year_picker);

        dayPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        monthPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        yearPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
    }

    public void show(String tag) {
        if (tag == null) throw new IllegalArgumentException("tag must not be null");
        show();
        this.tag = tag;
    }

    /**
     * Показывает диалог с выбором даты
     */
    public void show() {
        if (dialog.isShowing())
            dialog.cancel();
        tag = DEFAULT_TAG;
        switch (choiceMod) {
            case Calendar.DATE:
                dayPicker.setVisibility(View.VISIBLE);
                monthPicker.setVisibility(View.VISIBLE);
                yearPicker.setVisibility(View.VISIBLE);
                break;
            case Calendar.MONTH:
                dayPicker.setVisibility(View.GONE);
                monthPicker.setVisibility(View.VISIBLE);
                yearPicker.setVisibility(View.VISIBLE);
                break;
            case Calendar.YEAR:
                dayPicker.setVisibility(View.GONE);
                monthPicker.setVisibility(View.GONE);
                yearPicker.setVisibility(View.VISIBLE);
                break;
        }
        initYearPicker();
        dialog.show();
        updateTitleText();
    }

    public void cancel() {
        dialog.cancel();
    }

    public void setDate(int day, int month, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, day);
        setDate(cal);
    }

    public void setDate(Calendar c) {
        if (!dates.isEmpty()) {

            CalendarHelper.initCalendarFields(c, choiceMod, 1);

            CalendarHelper.initCalendarFields(dates.get(0), choiceMod, 1);
            if (c.before(dates.get(0))) {
                initDate(dates.get(0));
                return;
            }
            CalendarHelper.initCalendarFields(dates.get(dates.size() - 1), choiceMod, 1);
            if (c.after(dates.get(dates.size() - 1))) {
                initDate(dates.get(dates.size() - 1));
                return;
            }

            for (int i = 0; i < dates.size(); i++) {
                CalendarHelper.initCalendarFields(dates.get(i), choiceMod, 1);

                if (c.compareTo(dates.get(i)) == 0) {
                    initDate(dates.get(i));
                    return;
                }
            }
        }
    }

    private void initDate(Calendar cal) {
        selectedDay = cal.get(Calendar.DAY_OF_MONTH);
        selectedMonth = cal.get(Calendar.MONTH);
        selectedYear = cal.get(Calendar.YEAR);
    }

    /**
     * Заполнение numberPicker (день) значениями из БД за определенный месяц и год.
     * Назначение минимального и максимального индекса.
     * Запрет на бесконечную прокрутку.
     * Установка changeListner (при прокрутке списка метод onValueChange
     * инициализирует переменную selectedDay).
     */
    private void initDayPicker(final List<Integer> days) {
        dayPicker.invalidate();
        dayPicker.setDisplayedValues(null);
        dayPicker.setMinValue(0);
        dayPicker.setMaxValue(days.size() - 1);
        dayPicker.setWrapSelectorWheel(false);
        dayPicker.setDisplayedValues(convertIntegerIntoString(days));
        dayPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int lastId, int currentId) {
                selectedDay = days.get(currentId);
                updateTitleText();
            }
        });
        dayPicker.setValue(days.indexOf(selectedDay));
        selectedDay = days.get(dayPicker.getValue());
    }

    /**
     * Заполнение numberPicker (месяц) значениями из БД за определенный год.
     * Назначение минимального и максимального индекса.
     * Запрет на бесконечную прокрутку.
     * Установка changeListner (при прокрутке списка метод onValueChange
     * инициализирует переменную selectedMonth и перерисовывает dayPicker).
     */
    private void initMonthPicker(final List<Integer> months, final int year) {
        monthPicker.invalidate();
        monthPicker.setDisplayedValues(null);
        monthPicker.setMinValue(0);
        monthPicker.setMaxValue(months.size() - 1);
        monthPicker.setWrapSelectorWheel(false);
        monthPicker.setDisplayedValues(convertMonthIdIntoNames(months));
        monthPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int lastId, int currentId) {
                selectedMonth = months.get(currentId);
                if (choiceMod == Calendar.DATE)
                    initDayPicker(getDays(selectedMonth, year));
                updateTitleText();
            }
        });
        monthPicker.setValue(months.indexOf(selectedMonth));
        selectedMonth = months.get(monthPicker.getValue());

        if (choiceMod == Calendar.DATE)
            initDayPicker(getDays(selectedMonth, year));
    }

    /**
     * Заполнение numberPicker (год) значениями из БД
     * Назначение минимального и максимального индекса.
     * Запрет на бесконечную прокрутку.
     * Установка changeListner (при прокрутке списка метод onValueChange
     * инициализирует переменную selectedYear и перерисовывает monthPicker).
     */
    private void initYearPicker() {
        final List<Integer> years = getYear();

        monthPicker.invalidate();
        monthPicker.setDisplayedValues(null);
        yearPicker.setMinValue(0);
        yearPicker.setMaxValue(years.size() - 1);
        yearPicker.setWrapSelectorWheel(false);
        yearPicker.setDisplayedValues(convertIntegerIntoString(years));
        yearPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int lastId, int currentId) {
                selectedYear = years.get(currentId);
                if (choiceMod == Calendar.DATE || choiceMod == Calendar.MONTH)
                    initMonthPicker(getMonth(selectedYear), selectedYear);
                updateTitleText();
            }
        });
        yearPicker.setValue(years.indexOf(selectedYear));
        selectedYear = years.get(yearPicker.getValue());

        if (choiceMod == Calendar.DATE || choiceMod == Calendar.MONTH)
            initMonthPicker(getMonth(selectedYear), selectedYear);
    }

    /**
     * Делает выборку дней по месяцу и году
     */
    public List<Integer> getDays(int month, int year) {
        List<Integer> days = new ArrayList<Integer>();
        for (Calendar date : dates) {
            if (date.get(Calendar.YEAR) == year && date.get(Calendar.MONTH) == month) {
                if (days.isEmpty())
                    days.add(date.get(Calendar.DAY_OF_MONTH));
                if (days.get(days.size() - 1) != date.get(Calendar.DAY_OF_MONTH))
                    days.add(date.get(Calendar.DAY_OF_MONTH));

            } else if (!days.isEmpty())
                break;
        }
        return days;
    }

    /**
     * Делает выборку месяцев по году
     */
    public List<Integer> getMonth(int year) {
        List<Integer> months = new ArrayList<Integer>();
        for (Calendar date : dates) {
            if (date.get(Calendar.YEAR) == year) {
                if (months.isEmpty())
                    months.add(date.get(Calendar.MONTH));
                if (months.get(months.size() - 1) != date.get(Calendar.MONTH))
                    months.add(date.get(Calendar.MONTH));

            } else if (!months.isEmpty())
                break;
        }
        return months;
    }

    /**
     * Делает выборку годов
     */
    private List<Integer> getYear() {
        List<Integer> years = new ArrayList<Integer>();
        years.add(dates.get(0).get(Calendar.YEAR));
        for (Calendar date : dates) {
            if (years.get(years.size() - 1) != date.get(Calendar.YEAR)) {
                years.add(date.get(Calendar.YEAR));
            }
        }
        return years;
    }

    private void updateTitleText() {
        switch (choiceMod) {
            case Calendar.DATE:
                dialog.setTitle(selectedDay + " " + CalendarHelper.getMonthName(context, selectedMonth) + " " + selectedYear);
                break;
            case Calendar.MONTH:
                dialog.setTitle(CalendarHelper.getMonthName(context, selectedMonth) + " " + selectedYear);
                break;
            case Calendar.YEAR:
                dialog.setTitle(selectedYear + " ");
                break;
        }
    }

    /**
     * Оповещение слушателей об изменении даты
     */
    public void updateDate() {
        listener.updateDate(tag, selectedDay, selectedMonth + 1, selectedYear);
    }

    /**
     * Преобразует номера месяцев (0-11) в полные локализованные названия (из string.xml)
     *
     * @return массив месяцев с полными названиями
     */
    private String[] convertMonthIdIntoNames(List<Integer> months) {
        String[] allNames = context.getResources().getStringArray(R.array.gdmnlib_short_month_list_values);
        String[] resultNames = new String[months.size()];
        for (int i = 0; i < months.size(); i++)
            resultNames[i] = allNames[months.get(i)];
        return resultNames;
    }

    private String[] convertIntegerIntoString(List<Integer> integers) {
        String[] resultYears = new String[integers.size()];
        int j = 0;
        for (Integer i : integers) {
            resultYears[j] = String.valueOf(i);
            j++;
        }
        return resultYears;
    }

    public void setDates(List<Calendar> dates) {
        this.dates = dates;
        Collections.sort(dates, new Comparator<Calendar>() {
            @Override
            public int compare(Calendar lhs, Calendar rhs) {
                return lhs.compareTo(rhs);
            }
        });
    }

    public List<Calendar> getDates() {
        return dates;
    }

    public String getTag() {
        return tag;
    }
}
