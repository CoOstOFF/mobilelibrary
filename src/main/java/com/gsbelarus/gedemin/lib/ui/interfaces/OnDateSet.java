package com.gsbelarus.gedemin.lib.ui.interfaces;

public interface OnDateSet {
    void updateDate(String tag, int day, int month, int year);
}