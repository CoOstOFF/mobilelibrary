package com.gsbelarus.gedemin.lib.sync.protocol;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;

import java.util.Calendar;

/**
 * Запуск синзронизации по расписанию через сервис.
 */
abstract public class AlarmReceiver extends BroadcastReceiver {

    public static final String ALARM_SYNC_START = "alarm_sync_start";
    public static final int DEFAULT_INTERVAL_UPDATE = 30;
    private static final int PENDING_REQUEST_CODE = 1;
    private Context context;
    private int intervalUpdateInMinutes = DEFAULT_INTERVAL_UPDATE;
    private PendingIntent pendingIntent;

    /**
     * Обычно вызывается после вызова метода {@link #startSync()}.
     * Может быть вызван без вызова метода {@link #startSync()}.
     * Внутри метода можно вызвать {@link #setIntervalUpdateInMinutes(int)}
     *
     * @param oldDate последняя дата на которой была синхронизация
     * @return дата седующей синхронизации
     */
    abstract public Calendar getNextSyncDate(Calendar oldDate);

    /**
     * Внутри следуе начать синхронизацию
     * Вызывается перед вызовом метода {@link #getNextSyncDate(Calendar)}
     */
    abstract public void startSync();

    /**
     * @return в демо режиме автосинхронизация не будет срабатывать
     */
    abstract public boolean isWorkMode();

    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtil.d("do");
        this.context = context;
        if (intent.getBooleanExtra(ALARM_SYNC_START, false)) {

            pendingIntent = getPendingIntent(PendingIntent.FLAG_NO_CREATE);
            if (pendingIntent != null)
                pendingIntent.cancel();

            if (isWorkMode())
                startSync();
        }

        Calendar date = Calendar.getInstance();
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        setUpdate(getNextSyncDate(date));
    }

    private void setUpdate(Calendar cal) {
        /** создаем PendingIntent который затрет предыдущий */
        pendingIntent = getPendingIntent(PendingIntent.FLAG_NO_CREATE);
        if (pendingIntent != null) {
            LogUtil.d("Дата и время следующего обновления: уже установлено");
            return;
        } else
            pendingIntent = getPendingIntent(PendingIntent.FLAG_CANCEL_CURRENT);

        /** выбираем случайное время (в перделах DEFAULT_INTERVAL_UPDATE), что бы не нагружать сервер*/
        cal.add(Calendar.MINUTE, (int) (Math.random() * intervalUpdateInMinutes));

        /** выставляем время в будильнике */
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);

        LogUtil.d("Дата и время следующего обновления:", cal);
    }

    /**
     * получить pendingIntent с определенным флагом
     * *********************************
     * PendingIntent.FLAG_NO_CREATE - если в системе есть pendingIntent с таким intent, то возвращает его.
     * если в системе нет pendingIntent с таким intent, возвращает null
     * *********************************
     * PendingIntent.FLAG_CANCEL_CURRENT - если в системе уже есть pendingIntent с таким intent, pendingIntent полностью пересоздается
     * *********************************
     * PendingIntent.FLAG_UPDATE_CURRENT - если в системе уже есть pendingIntent с таким intent, новый не создается,
     * а его intent помещается в старый
     * *********************************
     */
    private PendingIntent getPendingIntent(int flag) {
        return PendingIntent.getBroadcast(context, PENDING_REQUEST_CODE, new Intent(context, this.getClass()).putExtra(ALARM_SYNC_START, true), flag);
    }

    public Context getContext() {
        return context;
    }

    public int getIntervalUpdateInMinutes() {
        return intervalUpdateInMinutes;
    }

    /**
     * Этот интервал используется для рандомной отсрочки синхронизации по времени
     * (нужно что бы не нагружать сервер в случае одновременной синхронизации большого количества пользователей)
     *
     * @param intervalUpdateInMinutes интервал в минутах
     */
    public void setIntervalUpdateInMinutes(int intervalUpdateInMinutes) {
        this.intervalUpdateInMinutes = intervalUpdateInMinutes;
    }
}
