package com.gsbelarus.gedemin.lib.sync.protocol.etc;

import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс для соединения с сервером и скачки одного блока данных
 */
final public class DownloadHelper {

    private List<String> line;
    private List<OnChangeProgress> listener;

    private boolean isProgress;

    public interface OnChangeProgress {
        void onChangeProgress(int percent);
    }

    public DownloadHelper(OnChangeProgress listener) {
        this.line = new ArrayList<>();
        this.listener = new ArrayList<>();
        this.listener.add(listener);
    }

    /**
     * Публичный метод для загрузки блока данных
     *
     * @param urlString  строка url для соединения. Преобразуется в объект класса URL
     * @param isProgress если true - состояние закачки блока будет отображатся с помощью интерфейса OnChangeProgress,
     *                   если false - состояние закачки блока отображатся не будет
     * @return List считанных данных
     * @throws IOException
     */
    public List<String> download(String urlString, boolean isProgress) throws IOException, IllegalArgumentException {
        this.isProgress = isProgress;
        if (urlString == null)
            throw new IOException("urlString is null");
        LogUtil.d("URL_STRING", urlString);
        return downloadData(new URL(urlString));
    }

    /**
     * Метод загрузки блока данных. Вызывается из публичного метода download
     *
     * @param url url сервера
     * @return List считанных данных
     * @throws IOException
     */
    private List<String> downloadData(URL url) throws IOException, IllegalArgumentException {
        line.clear();
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        BufferedReader reader = null;
        int totalSize;
        int downloadedSize = 0;
        int progress = 0;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setConnectTimeout(60 * 1000);
            urlConnection.setReadTimeout(60 * 1000);
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(false);
            urlConnection.connect();

            inputStream = urlConnection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(inputStream, "cp1251"));
            totalSize = urlConnection.getContentLength();

            String buf;
            while ((buf = reader.readLine()) != null) {

                line.add(buf);

                if (isProgress) {
                    downloadedSize += buf.length();
                    int progressBuf = (int) ((downloadedSize / (float) totalSize) * 100);
                    if (progressBuf - progress >= 1) {
                        progress = progressBuf;
                        changeProgress(progress);
                    }
                }
            }
        } finally {
            if (inputStream != null)
                inputStream.close();
            if (reader != null)
                reader.close();
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return line;
    }

    /**
     * Оповещение слушателя о изменении статуса загрузки одного блока
     *
     * @param progress процент загрузки
     */
    private void changeProgress(int progress) {
        for (OnChangeProgress l : listener)
            l.onChangeProgress(progress);
    }
}
