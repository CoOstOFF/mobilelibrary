package com.gsbelarus.gedemin.lib.sync.protocol.entity;

import java.io.Serializable;

public class SyncServiceTask<T> implements Serializable {

    public enum TypeOfTask {
        SINGLE_SYNC, SERIES_SYNC, REQUEST_STATUS_SYNC, STOP_SYNC
    }

    private TypeOfTask typeOfTask;
    private T subTask;
    private boolean backgroundTask;
    private String successfulMessage;

    public SyncServiceTask(TypeOfTask typeOfTask) {
        this.typeOfTask = typeOfTask;
        this.backgroundTask = false;
    }

    public SyncServiceTask(TypeOfTask typeOfTask, T subTask) {
        this.subTask = subTask;
        this.typeOfTask = typeOfTask;
        this.backgroundTask = false;
    }

    public SyncServiceTask(TypeOfTask typeOfTask, boolean backgroundTask) {
        this.typeOfTask = typeOfTask;
        this.backgroundTask = backgroundTask;
    }

    public SyncServiceTask(TypeOfTask typeOfTask, String successfulMessage) {
        this.typeOfTask = typeOfTask;
        this.successfulMessage = successfulMessage;
    }

    public SyncServiceTask(TypeOfTask typeOfTask, T subTask, boolean backgroundTask) {
        this.typeOfTask = typeOfTask;
        this.subTask = subTask;
        this.backgroundTask = backgroundTask;
    }

    public SyncServiceTask(TypeOfTask typeOfTask, T subTask, String successfulMessage) {
        this.typeOfTask = typeOfTask;
        this.subTask = subTask;
        this.successfulMessage = successfulMessage;
    }

    public T getSubTask() {
        return subTask;
    }

    public SyncServiceTask<T> setSubTask(T subTask) {
        this.subTask = subTask;
        return this;
    }

    public TypeOfTask getTypeOfTask() {
        return typeOfTask;
    }

    public boolean isBackgroundTask() {
        return backgroundTask;
    }

    public SyncServiceTask<T> setBackgroundTask(boolean backgroundTask) {
        this.backgroundTask = backgroundTask;
        return this;
    }

    public String getSuccessfulMessage() {
        return successfulMessage;
    }

    public SyncServiceTask setSuccessfulMessage(String successfulMessage) {
        this.successfulMessage = successfulMessage;
        return this;
    }
}
