package com.gsbelarus.gedemin.lib.sync.protocol.etc;

import com.google.gson.Gson;
import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncHeader;
import com.gsbelarus.gedemin.lib.sync.protocol.exception.InvalidResponseException;
import com.gsbelarus.gedemin.lib.ui.utils.LogUtil;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Анализатор данных
 */
final public class Parser {

    public static final String BLOCK_NUM = "block_num";

    public static final String EMPTY_RESPONSE_EXCEPTION = "Ответ сервера: null";

    /**
     * Метод для анализа заголовка блока.
     * Вид заголовка:
     * <br> {"cl":"SyncHeader", "params":{"{@link Parser#BLOCK_NUM}":"value","key":"value",...}}
     *
     * @param lines блок данных
     * @return параметр = значение.
     * @throws InvalidResponseException
     */
    public HashMap<String, String> parsingHeader(List<String> lines) throws InvalidResponseException {
        try {
            if (lines.isEmpty())
                throw new InvalidResponseException(EMPTY_RESPONSE_EXCEPTION);

            Gson gson = new Gson();
            SyncHeader syncHeader = gson.fromJson(lines.get(0), SyncHeader.class);

            if (syncHeader.getCl() == null || !syncHeader.getCl().equals(SyncHeader.class.getSimpleName()))
                throw new InvalidResponseException("Некорректный ответ сервера");

            HashMap<String, String> headerMap = new LinkedHashMap<>();
            if (syncHeader.getParams() != null)
                headerMap.putAll(syncHeader.getParams());

            return headerMap;

        } catch (Exception e) {
            throw new InvalidResponseException(e.getMessage());
        }
    }

    public void removeHeaderFromList(List<String> lines) {
        try {
            if (!lines.isEmpty()) {
                Gson gson = new Gson();
                SyncHeader syncHeader = gson.fromJson(lines.get(0), SyncHeader.class);
                if (syncHeader != null && syncHeader.getCl() != null && syncHeader.getCl().equals(SyncHeader.class.getSimpleName()))
                    lines.remove(0);
            }
        } catch (Exception e) {
            LogUtil.e(e);
        }
    }
}
