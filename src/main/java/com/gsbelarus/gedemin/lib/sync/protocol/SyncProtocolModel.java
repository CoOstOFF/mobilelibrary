package com.gsbelarus.gedemin.lib.sync.protocol;

import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncStatus;
import com.gsbelarus.gedemin.lib.sync.protocol.exception.CustomSyncException;

import java.util.HashMap;
import java.util.List;

public interface SyncProtocolModel {

    /**
     * Выполняется во время подготовки к созданию нового потока загрузки, имеет доступ к UI потоку
     */
    void onPreExecute();

    /**
     * Обновление состояния загрузки
     *
     * @param percent      процент загрузки
     * @param currentBlock загружаемый блок
     * @param totalBlocks  общее количество блоков
     */
    void onProgressUpdate(int percent, int currentBlock, int totalBlocks);

    /**
     * Выполняется после загрузки, имеет доступ к UI потоку
     * внутри следует обрабатывать ошибки
     *
     * @param status статус с каким завершилась синхронизация
     *               (NO_INTERNET_CONNECTION, INVALID_RESPONSE, EMPTY_RESPONSE, TIMEOUT, NOT_REQUIRED,
     *               ELSE_FAILED, SUCCESSFUL, STOP_SYNC, CUSTOM_SYNC_FAILED)
     */
    void onPostExecute(SyncStatus status);

    /**
     * Открыть транзакцию, выполняется в потоке загрузки
     */
    void onBeginTransaction();

    /**
     * Вызывается после получения ответа, в потоке загрузки.
     * Для вывода ошибки необходимо сгененировать исключение CustomSyncException("локализованное название ошибки")
     *
     * @param headerMap массив праметров из заголовка.
     * @throws CustomSyncException прерывает загрузку данных, отображает диолог об ошибке
     */
    void onHeaderLoaded(HashMap<String, String> headerMap) throws CustomSyncException;

    /**
     * Вызывается после получения ответа, в потоке загрузки.
     * Для вывода ошибки необходимо сгененировать исключение CustomSyncException("локализованное название ошибки")
     *
     * @param lines тело блока в виде строк. Следует распарсить и вставить в БД.
     * @throws CustomSyncException прерывает загрузку данных, отображает диолог об ошибке
     */
    void onBlockLoaded(List<String> lines) throws CustomSyncException;

    /**
     * Закрыть транзакцию успешно, выполняется в потоке загрузки
     */
    void onEndTransaction();

    /**
     * Закрыть транзакцию с откатом изменений, выполняется в потоке загрузки
     */
    void onCancelTransaction();
}
