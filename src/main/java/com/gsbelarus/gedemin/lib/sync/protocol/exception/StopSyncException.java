package com.gsbelarus.gedemin.lib.sync.protocol.exception;

import java.io.IOException;

public class StopSyncException extends IOException {

    public StopSyncException() {
        super();
    }
}
