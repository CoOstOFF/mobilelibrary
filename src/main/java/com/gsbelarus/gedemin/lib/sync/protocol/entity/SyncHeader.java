package com.gsbelarus.gedemin.lib.sync.protocol.entity;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.HashMap;

public class SyncHeader implements Serializable {

    @Expose
    private String cl;
    @Expose
    private HashMap<String, String> params;

    public String getCl() {
        return cl;
    }

    public void setCl(String cl) {
        this.cl = cl;
    }

    public HashMap<String, String> getParams() {
        return params;
    }

    public void setParams(HashMap<String, String> params) {
        this.params = params;
    }
}
