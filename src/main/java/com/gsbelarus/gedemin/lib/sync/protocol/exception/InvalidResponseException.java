package com.gsbelarus.gedemin.lib.sync.protocol.exception;

import java.io.IOException;

/**
 * некорректный ответ сервера
 */
public class InvalidResponseException extends IOException {

    /**
     * Constructs a new {@code IOException} with its stack trace and detail
     * message filled in.
     *
     * @param detailMessage the detail message for this exception.
     */
    public InvalidResponseException(String detailMessage) {
        super(detailMessage);
    }
}
