package com.gsbelarus.gedemin.lib.sync.protocol.entity;

import java.io.Serializable;

public class SyncStatus implements Serializable {

    /**
     * Статус завершения синхронизации
     * (NO_INTERNET_CONNECTION, INVALID_RESPONSE, EMPTY_RESPONSE, TIMEOUT,
     * NOT_REQUIRED, ELSE_FAILED, SUCCESSFUL, STOP_SYNC, CUSTOM_SYNC_FAILED)
     */
    public enum TypeOfStatus {
        /**
         * Нет подключения к интернет
         */
        NO_INTERNET_CONNECTION,

        /**
         * Некорректный ответ сервера
         */
        INVALID_RESPONSE,

        /**
         * Пустой ответ сервера
         */
        EMPTY_RESPONSE,

        /**
         * Таймаут (30 сек)
         */
        TIMEOUT,

        /**
         * Обновление не требуется, возникает если макс версия равна текущей
         */
        NOT_REQUIRED,

        /**
         * Другие ошибки
         */
        ELSE_FAILED,

        /**
         * Синхронизация завершена успешно
         */
        SUCCESSFUL,

        /**
         * Синхронизация остановлена
         */
        STOP_SYNC,

        /**
         * Пользовательская ошибка, можно сгенерировать в момент анализа данных
         */
        CUSTOM_SYNC_FAILED
    }

    private TypeOfStatus typeOfStatus;
    private String message = "";

    public SyncStatus(TypeOfStatus typeOfStatus) {
        this.typeOfStatus = typeOfStatus;
    }

    public SyncStatus(TypeOfStatus typeOfStatus, String message) {
        this.typeOfStatus = typeOfStatus;
        this.message = message;
    }

    public TypeOfStatus getTypeOfStatus() {
        return typeOfStatus;
    }

    public String getMessage() {
        return message;
    }

    public SyncStatus setMessage(String message) {
        this.message = message;
        return this;
    }
}
