package com.gsbelarus.gedemin.lib.sync.protocol;

import android.app.Notification;

import com.gsbelarus.gedemin.lib.sync.protocol.entity.SyncStatus;

public interface SyncNotificationAdapter {

    int getForegroundId();

    Notification buildStartNotification(boolean isWorkMode);

    void showUpdatingNotification();

    void showDownloadingNotification(int percent, int currentBlock, int totalBlocks);

    void showSyncStatusNotification(SyncStatus status);

    void clearForegroundNotification();
}