package com.gsbelarus.gedemin.lib.sync.protocol.exception;

import java.io.IOException;

/**
 * обновление не требуется
 */
public class NotRequiredException extends IOException {

    public static final String MESSAGE = "NotRequired";

    public NotRequiredException() {
        super(MESSAGE);
    }
}
