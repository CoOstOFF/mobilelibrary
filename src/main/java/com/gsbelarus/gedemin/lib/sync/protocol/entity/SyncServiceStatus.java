package com.gsbelarus.gedemin.lib.sync.protocol.entity;

import java.io.Serializable;

public class SyncServiceStatus<T> implements Serializable {

    public enum TypeOfServiceStatus {
        START_SYNC, FINISH_SYNC
    }

    private TypeOfServiceStatus typeOfServiceStatus;
    private SyncStatus status = new SyncStatus(SyncStatus.TypeOfStatus.SUCCESSFUL);
    private SyncServiceTask<T> task;

    public TypeOfServiceStatus getTypeOfServiceStatus() {
        return typeOfServiceStatus;
    }

    public SyncServiceStatus<T> setTypeOfServiceStatus(TypeOfServiceStatus typeOfServiceStatus) {
        this.typeOfServiceStatus = typeOfServiceStatus;
        return this;
    }

    public SyncStatus getStatus() {
        return status;
    }

    public SyncServiceStatus<T> setStatus(SyncStatus status) {
        this.status = status;
        return this;
    }

    public SyncServiceTask<T> getTask() {
        return task;
    }

    public SyncServiceStatus<T> setTask(SyncServiceTask<T> task) {
        this.task = task;
        return this;
    }
}
